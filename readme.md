[![pipeline status](https://gitlab.com/p1892/rustdoc-assets/badges/master/pipeline.svg)](https://gitlab.com/p1892/rustdoc-assets/-/commits/master)

Build script helper which copies media and assets from a source directory below your
current crate to the target output directory of rustdoc.

Check out the documentation on GitLab-Pages: https://p1892.gitlab.io/rustdoc-assets
