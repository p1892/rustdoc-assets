// bootstrap crate
#[path = "src/lib.rs"]
mod rustdoc_assets;

fn main() {
    rustdoc_assets::copy_assets_folder("doc/img");
}
