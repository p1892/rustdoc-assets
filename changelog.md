# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## 0.2.1 - 2022-11-16

### Fixed
- Remove unnecessary recompilation.

### Changed
- Bump Rust edition from 2018 to 2021.


## 0.2.0 - 2021-12-04

### Add

- Add support for workspaces by checking the environment variable `CARGO_WORKSPACE_DIR`.


## 0.1.2 - 2021-10-16

### Fixed

- Fixed repository URL points to public group in GitLab.
- Add comment on new behavor of cargo doc.


## 0.1.1 - 2020-12-18

### Fixed

- Add support for alternate output directories.

  The output directory for [rustdoc](https://doc.rust-lang.org/cargo/commands/cargo-rustdoc.html#output-options) can be overwritten by setting the environment variable `CARGO_TARGET_DIR`. `rustdoc-assets` now honors this variable.

## [Unreleased]
